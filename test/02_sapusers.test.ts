import request from "supertest";
import app from "../src/index";
import mysql from "../src/config/mysql";
import SapUserBranch from "../src/models/SapUserBranch";
const api = request(app);

beforeAll(() => {
    SapUserBranch.truncate({ force: true });
});

describe("Check the module [Users]", () => {

    test("get the list of users (index)", async () => {
        const response = await api.get('/b1s/v2/SapUsers');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
        expect(response.body).toHaveProperty('previousLink');
        expect(response.body).toHaveProperty('nextLink');
    })

    test("display the next page of the user list", async () => {
        const response = await api.get('/b1s/v2/SapUsers?skip=1&maxpagesize=2');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
        expect(response.body).toHaveProperty('previousLink');
        expect(response.body).toHaveProperty('nextLink');
    })

    test("get the user by id (show)", async () => {
        const response = await api.get('/b1s/v2/SapUsers/1');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
    })

    test("register user correctly (store)", async () => {
        const response = await api.post('/b1s/v2/SapUsers')
            .send({
                "username": 'test01',
                "password": 'test01',
                "first_name": 'test_name',
                "last_name": 'test_last_name',
                "email": 'test@dominio.com',
                "FederalTaxID": 'XEXX010101000',
                "rolId": null,
                "branches": [
                    {
                        "ShipToCode": "COLIMA"
                    },
                    {
                        "ShipToCode": "JALISCO"
                    }
                ],
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(201);
        expect(response.body).toHaveProperty('status', 201);
        expect(response.body).toHaveProperty('msg', 'ok');
    })

    test("update user correctly (update)", async () => {
        const response = await api.patch('/b1s/v2/SapUsers/1')
            .send({
                "password": "",
                "first_name": "test_update",
                "last_name": "test_last_update",
                "email": "test@update.com",
                "active": "0",
                "FederalTaxID": "XEXX010101000",
                "rolId": null,
                "branches": [
                    {
                        "ShipToCode": "VERACRUZ"
                    }
                ],
            }).set('Accept', 'application/json');

        expect(response.status).toEqual(204);
    })

    test("should return error trying to update user.", async () => {
        const response = await api.patch('/b1s/v2/SapUsers/1')
            .send({
                "active": "2"
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(500);
        expect(response.body).toHaveProperty('status', 500);
        expect(response.body).toHaveProperty('msg');
        expect(response.body).toHaveProperty('value');
    })

    test("should return user is not found", async () => {
        const response = await api.get('/b1s/v2/SapUsers/32');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(404);
        expect(response.body).toHaveProperty('status', 404);
        expect(response.body).toHaveProperty('msg');
        expect(response.body).toHaveProperty('value');
    })
})

afterAll(() => {
    mysql.close();
})