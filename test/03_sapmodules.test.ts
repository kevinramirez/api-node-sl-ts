import request from "supertest";
import app from "../src/index";
import mysql from "../src/config/mysql";
import SapModules from "../src/models/SapModules";
const api = request(app);

beforeAll(() => {
    SapModules.truncate({ force: true });
})

describe("Check the module [Sap Modules]", () => {
    test("correctly create a module (store)", async () => {
        const response = await api.post('/b1s/v2/SapModules')
            .send({
                "name": "module test"
            }).set('content-type', 'application/json');

        expect(response.headers['content-type']).toMatch(/application\/json/);
        expect(response.status).toEqual(201);
        expect(response.body).toHaveProperty('status', 201);
        expect(response.body).toHaveProperty('msg', 'ok');
    })

    test("get the list of modules (index)", async () => {
        const response = await api.get('/b1s/v2/SapModules');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.statusCode).toEqual(200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
        expect(response.body).toHaveProperty('previousLink');
        expect(response.body).toHaveProperty('nextLink');
    })

    test("get the module by id (show)", async () => {
        const response = await api.get('/b1s/v2/SapModules/1');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
    })
})

afterAll(() => {
    mysql.close();
})