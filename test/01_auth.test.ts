import request from "supertest";
import app from "../src/index";
import mysql from "../src/config/mysql";
import SapUsers from "../src/models/SapUsers";
const api = request(app);

beforeAll(() => {
    SapUsers.destroy({
        where: {},
        truncate: true
    });
});

describe("Check the route [AUTH].", () => {

    test("should register the user correctly.", async () => {
        const response = await api.post('/b1s/v2/auth/register')
            .send({
                username: 'test',
                password: 'test'
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
    })

    test("should login correctly (200).", async () => {
        const response = await api.post('/b1s/v2/auth/login')
            .send({
                username: 'test',
                password: 'test',
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('status', 200);
        expect(response.body).toHaveProperty('msg', 'ok');
        expect(response.body).toHaveProperty('data');
    })

    test("should return user is not found (404)", async () => {
        const response = await api.post('/b1s/v2/auth/login')
            .send({
                "username": "other",
                "password": "other",
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(404);
        expect(response.body).toHaveProperty('status', 404);
        expect(response.body).toHaveProperty('msg');
        expect(response.body).toHaveProperty('value');
    })

    test("should return error username already in use.", async () => {
        const response = await api.post('/b1s/v2/auth/register')
            .send({
                "username": "test",
                "password": "test",
            }).set('Accept', 'application/json');

        expect(response.headers["content-type"]).toMatch(/application\/json/);
        expect(response.status).toEqual(500);
        expect(response.body).toHaveProperty('status', 500);
        expect(response.body).toHaveProperty('msg');
        expect(response.body).toHaveProperty('value');
    })
})

afterAll(() => {
    mysql.close();
})