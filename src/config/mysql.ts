import { Sequelize } from 'sequelize';

const { NODE_ENV, DATABASE_MYSQL, USER_MYSQL, PASSWORD_MYSQL, SERVER_MYSQL, DATABASE_MYSQL_TEST, TIMEZONE_MYSQL } = process.env;

const database = (NODE_ENV === 'test' ? DATABASE_MYSQL_TEST : DATABASE_MYSQL) || "";
const username = USER_MYSQL || "root";
const password = PASSWORD_MYSQL || "";
const host = SERVER_MYSQL || "localhost";

const sequelize = new Sequelize(database, username, password, {
  host: host,
  dialect: 'mysql',
  timezone: TIMEZONE_MYSQL || "-06:00"
})

sequelize.authenticate()
.then(() => {
  console.log('Connection has been established successfully. (MySQL)');
})
.catch(err => {
  console.error('Unable to connect to the database (MySQL):', err);
});

export default sequelize;