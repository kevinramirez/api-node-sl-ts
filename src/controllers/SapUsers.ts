import { Request, Response } from "express";
import { httpError } from '../helpers/handleError';
import { encrypt } from '../helpers/handleBcrypt';
import { getSapUsersAll, getSapUserById, createSapUser, updateSapUser, createUserBranches, deleteUserBranch } from "../services/SapUsers";

const index = async (req: Request, res: Response): Promise<void> => {
    try {
        const skip: number = parseInt(req.query.skip as string);
        const maxpagesize: number = parseInt(req.query.maxpagesize as string);

        const users = await getSapUsersAll(skip, maxpagesize);

        res.status(200).send(users);

    } catch (error: any) {
        httpError(res, 500, 'Error while finding User: \n' + error.message);
    }
}

const show = async (req: Request, res: Response): Promise<void> => {
    const id: number = parseInt(req.params.id as string);

    try {
        const user = await getSapUserById(id);

        if (!user) return httpError(res, 404, "No matching records found");

        res.status(200).send({
            "status": 200,
            "msg": 'ok',
            "data": user
        });
    } catch (error: any) {
        httpError(res, 500, 'Error while finding User: \n ' + error.message);
    }
}

const store = async (req: Request, res: Response): Promise<void> => {
    const { username, password, first_name, last_name, email, FederalTaxID, branches, rolId } = req.body;

    try {

        const passwordHash = (password) ? await encrypt(password) : password;

        const user = await createSapUser({
            username,
            password: passwordHash,
            first_name,
            last_name,
            email,
            FederalTaxID,
            // FederalTaxID: FederalTaxID ? FederalTaxID : req.userdata?.['_FederalTaxID'],
            rolId,
            branches,
        });

        res.status(201).send({
            "status": 201,
	        "msg": "ok",
        });
    } catch (error: any) {
        const errObj: Array<any> = [];

        if (error.errors) {
            error.errors.map((err: any) => {
                errObj.push({"msg": err.message, "value": err.value, "param": err.path});
            });            
        }

        httpError(res, 500, "Some error occurred while creating the User. \n " + error.message, errObj);
    }
}

const update = async (req: Request, res: Response): Promise<void> => {
    try {
        const id: number = parseInt(req.params.id as string);
        const { password, email, active, first_name, last_name, rolId, branches } = req.body;

        const user = await getSapUserById(id);

        if (!user) return httpError(res, 404, `User ${id} does not exist.`);

        const updateUser = await updateSapUser(id, {
            password: password ? await encrypt(password) : undefined,
            email,
            active,
            first_name,
            last_name,
            rolId,
        });

        // Add Permissions Branches
        if (branches) {
            await deleteUserBranch(id);

            const usersBranches = branches.map((branch: any) => {
                return { users_id: id,  ShipToCode: branch.ShipToCode }
            });

            await createUserBranches(usersBranches);
        }

        res.status(204).send();
    } catch (error: any) {
        const errObj: Array<any> = [];

        if (error.errors) {
            error.errors.map((err: any) => {
                errObj.push({"msg": err.message, "value": err.value, "param": err.path});
            });
        }

        httpError(res, 500, "Some error occurred while updated the User. \n " + error.message, errObj);
    }

}

const destroy = (req: Request, res: Response) => {}

const groupByModule = (collection: Array<any>, module: string): Array<any> => {
    let i = 0, val, index,
    temp = [], result = [];
    for (; i < collection.length; i++) {
        val = collection[i][module].name;
        index = temp.indexOf(val); // search temporary array
        if (index > -1)
            // if already exists, add action
            result[index]['actions'].push(collection[i].name.toUpperCase());
        else {
            temp.push(val); // add temporary array
            result.push({name: collection[i].module.name.toUpperCase(), actions: [collection[i].name.toUpperCase()]}); // add module with your action
        }
    }

    return result;
}

export { index, show, store, update, destroy }
