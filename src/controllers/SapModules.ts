import { Request, Response } from "express";
import { httpError } from "../helpers/handleError";
import { getSapModulesAll, getSapModuleById, createSapModule } from "../services/SapModules";

const index = async (req: Request, res: Response): Promise<void> => {
    try {
        const skip: number = parseInt(req.query.skip as string);
        const maxpagesize: number = parseInt(req.query.maxpagesize as string);

        const sapModules = await getSapModulesAll(skip, maxpagesize);

        res.status(200).send(sapModules);
    } catch (error: any) {
        httpError(res, 500, 'Error while finding Module: \n' + error.message);
    } 
}

const show = async (req: Request, res: Response): Promise<void> => {
    try {
        const id: number = parseInt(req.params.id as string);

        const sapModule = await getSapModuleById(id);

        if (!sapModule) return httpError(res, 404, 'Module not found');

        res.status(200).send({
            "status": 200,
            "msg": "ok",
            "data": sapModule
        });
    } catch (error: any) {
        httpError(res, 500, 'Error while finding Module: \n' + error.message);
    }
}

const store = async (req: Request, res: Response): Promise<void> => {
    try {
        const sapModule = await createSapModule(req.body);

        res.status(201).send({
            "status": 201,
            "msg": "ok",
        });
    } catch (error: any) {
        const errObj: Array<any> = [];

        if (error.errors) {
            error.errors.forEach((err: any) => errObj.push({"msg": err.message, "value": err.value, "param": err.path}));
        }

        httpError(res, 500, "Some error occurred while creating the Module. \n " + error.message, errObj);
    }
}

export { index, show, store }