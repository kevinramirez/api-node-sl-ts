import { Request, Response } from "express";
import { compare, encrypt } from "../helpers/handleBcrypt";
import { httpError } from "../helpers/handleError";
import SapUsers from "../models/SapUsers";

const login = async (req: Request, res: Response): Promise<void> => {
    const { username, password, company } = req.body;

    try {
        const user = await SapUsers.findOne({ where: { username, active: '1' }});

        if (!user) return httpError(res, 404, 'User not found.');

        const checkPassword = await compare(password, user.getDataValue('password'));

        if (!checkPassword) return httpError(res, 401, 'Invalid password.');

        res.status(200).send({
            "status": 200,
            "msg": 'ok',
            "data": user
        });
    } catch (error: any) {
        httpError(res, 500, error.message);
    }
}

const register = async (req: Request, res: Response) => {
    const { username, password, first_name, last_name, email, FederalTaxID, rolId } = req.body;

    try {
        const passwordHash = password ? await encrypt(password) : password;

        const user = await SapUsers.create({
            username,
            password: passwordHash,
            email,
            first_name,
            last_name,
            FederalTaxID,
            rolId
        });

        res.status(200).send({
            "status": 200,
            "msg": 'ok'
        });
    } catch (error: any) {
        const errObj: Array<any> = [];

        if (error.errors) {
            error.errors.map((err: any) => {
                errObj.push({"msg": err.message, "value": err.value, "param": err.path});
            });
        }

        httpError(res, 500, 'Some error occurred while register the User: \n ' + error.message, errObj);
    }
}

export { login, register }