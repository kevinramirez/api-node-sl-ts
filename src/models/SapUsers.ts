import { DataTypes } from 'sequelize';
import mysql from '../config/mysql';
import SapUserBranch from './SapUserBranch';

const SapUsers = mysql.define('SapUsers', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    username: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: {
            name: 'Username args',
            msg: 'Username already in use.'
        },
        validate: {
            notEmpty: true,
        },
    },
    password: {
        type: DataTypes.STRING(255),
        allowNull: false,
        validate: {
            notNull: true,
            notEmpty: true,
        },
    },
    email: {
        type: DataTypes.STRING(100),
        validate: {
            isEmail: true,
        },
    },
    active: {
        type: DataTypes.ENUM('1', '0'),
        defaultValue: '1',
        validate: {
            isIn: {
                args: [['1', '0']],
                msg: "The field 'active' Must be '1' or '0'",
            }
        }
    },
    first_name: DataTypes.STRING(100),
    last_name: DataTypes.STRING(100),
    FederalTaxID: {
        type: DataTypes.STRING(100),
    },
    company: DataTypes.STRING(100),
    phone: DataTypes.STRING(20),
    rolId: {
        type: DataTypes.INTEGER,
        references: {
            model: "sap_roles",
            key: "id"
        },
        comment: 'Id del rol de la tabla de roles',
    },
    createdAt: {
        type: 'TIMESTAMP',
        defaultValue: DataTypes.NOW,
        allowNull: false,
        field: 'created'
    },
    updatedAt: {
        type: 'TIMESTAMP',
        defaultValue: DataTypes.NOW,
        allowNull: false,
        field: 'modified'
    }
}, {
    modelName: "sap_users",
    tableName: 'sap_users'
});

SapUsers.hasMany(SapUserBranch, {
    as: "branches",
    foreignKey: "users_id",
});

SapUserBranch.belongsTo(SapUsers, {foreignKey: "users_id", targetKey: "id"});

SapUsers.sync();

export default SapUsers;