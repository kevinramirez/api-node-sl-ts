import { DataTypes } from "sequelize";
import mysql from "../config/mysql";

const SapModules = mysql.define('SapModules', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: true,
        comment: 'Número secuencial exclusivo.'
    },
    name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        validate: {
            notNull: true,
            notEmpty: true,
        },
        unique: {
            name: 'Module args',
            msg: 'Module already exists.'
        },
        comment: 'Nombre del modulo.'
    },
}, {
    comment: 'Modulos a los cuales va a tener permiso el usuario.',
    timestamps: false,
    tableName: 'sap_modules',
});

SapModules.sync();

export default SapModules;