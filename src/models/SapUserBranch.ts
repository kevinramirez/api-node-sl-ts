import { DataTypes } from "sequelize";
import mysql from "../config/mysql";

const SapUserBranch = mysql.define('SapUserBranch', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        comment: 'Número secuencial exclusivo.',
    },
    users_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: true,
            notEmpty: true,
        },
        references: {
            model: "sap_users",
            key: "id"
        },
        comment: 'Id del usuario de la tabla de users',
    },
    ShipToCode: {
        type: DataTypes.STRING(100),
        allowNull: false,
        validate: {
            notNull: true,
            notEmpty: true,
        },
        comment: 'Codigo de la sucursal - direccion de envio',
    }
}, {
    timestamps: false,
    tableName: 'sap_user_branch',
    comment: 'Realacion de las sucursales o direcciones de envio que estan asignados al usuario.',
});

SapUserBranch.sync();

export default SapUserBranch;