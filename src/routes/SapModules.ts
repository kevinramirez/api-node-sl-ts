import express, { Router } from "express";
const router: Router = express.Router();
import { index, show, store } from "../controllers/SapModules";

router.get('/', index);

router.get('/:id', show);

router.post('/', store);

export { router }