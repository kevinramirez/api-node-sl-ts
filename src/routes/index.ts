import { readdirSync } from "fs";
import express, { Router } from "express";
const router: Router = express.Router();

const PATH_ROUTES = __dirname;

const removeExtension = (filename: string): string => {
    const cleanFileName = <string>filename.split('.').shift();
    return cleanFileName;
}

const loadRouter = async (file: string): Promise<void> => {
    const name = removeExtension(file);
    const skip = ['index'].includes(name);

    if (!skip) {
        /* const routerModule = await import(`./${file}`);
        router.use(`/${name}`, routerModule.router); */
        import(`./${file}`).then((routerModule) => {
            router.use(`/${name}`, routerModule.router);
        });
    }
}

readdirSync(PATH_ROUTES).filter((file: string) => loadRouter(file));

export default router;