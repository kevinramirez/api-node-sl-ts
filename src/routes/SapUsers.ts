import express, { Router } from "express";
const router: Router = express.Router();
import { index, show, store, update, destroy } from "../controllers/SapUsers";

router.get('/', index);

router.get('/:id', show);

router.post('/', store);

router.patch('/:id', update);

router.delete('/:id', destroy);

export { router }