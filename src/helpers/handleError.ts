import { Request, Response } from "express";

const httpError = (res: Response, status: number, message: string, value: Array<any> = [], data: Object = {}) => {
    res.status(status).send({
        "status": status,
        "msg": message,
        "value": value
    });
}

export { httpError }