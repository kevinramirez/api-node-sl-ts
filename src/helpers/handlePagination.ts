const getPagination = (page: number, size: number) => {
    const limit = size ? + size : 20;
    const offset = page ? page * limit : 0;
    return { limit, offset };
}

const getPagingData = (data: any, page: number, limit: number, nameModule: string) => {
    const { count: totalItems, rows: users } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);

    return {
        "status": 200,
        "msg": 'ok',
        "data": users,
        "previousLink": currentPage - 1 < 0 ? null : `${nameModule}?skip=${currentPage - 1}`,
        "nextLink": currentPage + 1 >= totalPages ? null : `${nameModule}?skip=${currentPage + 1}`,
        "currentPage": currentPage,
    };
}

export { getPagination, getPagingData }