import SapModules from "../models/SapModules";
import { SapModuleType } from "../types/SapModules";
import { getPagination, getPagingData } from "../helpers/handlePagination";

const getSapModulesAll = async (skip: number, maxPageSize: number) => {
	const { limit, offset } = getPagination(skip, maxPageSize);

	const sapModules = await SapModules.findAndCountAll({
		limit,
		offset,
		order: [['id', 'DESC']],
	});

	return getPagingData( sapModules, skip, limit, 'SapModules');
}

const getSapModuleById = async (id: number) => {
    return await SapModules.findByPk(id);
}

const createSapModule = async (SapModule: SapModuleType) => {
	return await SapModules.create(SapModule);
}

export { getSapModulesAll, getSapModuleById, createSapModule }