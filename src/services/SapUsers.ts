import SapUsers from "../models/SapUsers";
import SapUserBranch from "../models/SapUserBranch";
import { SapUserType, SapBranchesType } from "../types/SapUsers";
import { getPagination, getPagingData } from "../helpers/handlePagination";

const getSapUsersAll = async (skip: number, maxPageSize: number) => {
    const { limit, offset } = getPagination(skip, maxPageSize);

    const users = await SapUsers.findAndCountAll({
        limit,
        offset,
        order: [['id','DESC']],
        attributes: {
            exclude: ['password', 'company', 'phone', 'createdAt', 'updatedAt']
        },
    });

    return getPagingData( users, skip, limit, 'SapUsers');
}

const getSapUserById = async (id: number) => {
    const user = await SapUsers.findByPk(id,{
        attributes: {
            exclude: ['password', 'company', 'phone', 'createdAt', 'updatedAt']
        },
        // distinct: true,
        include: [
            {
                model: SapUserBranch,
                as: 'branches',
                attributes: ['ShipToCode']
            },
            /* {
                model: SapRoles,
                attributes: ['name'],
                include: [
                    {
                        model: SapActions,
                        through: {
                            attributes: []
                        },
                        include: [{
                            model: SapModules,
                        }]
                    }
                ]
            } */
        ],
        /* order: [ [ SapRoles, SapActions, SapModules, 'id', 'ASC' ] ] */
    });

    /* const modules = groupByModule(user.getDataValue('rol.actions'), "module");

    user.setDataValue('modules', modules);
    user.setDataValue('rol', user.getDataValue('rol.name')); */

    return user;
}

const createSapUser = async (SapUser: SapUserType) => {
    return await SapUsers.create(SapUser, { include: ["branches"]});
}

const updateSapUser = async (id: number, SapUser: SapUserType) => {
    return await SapUsers.update(SapUser, { where: { id } });
}

/** Sap Users Branches */
const createUserBranches = async (SapBranches: SapBranchesType[]) => {
    return await SapUserBranch.bulkCreate(SapBranches);
}

const deleteUserBranch = async (users_id: number) => {
    return await SapUserBranch.destroy({ where: { users_id } });
}

export { getSapUsersAll, getSapUserById, createSapUser, updateSapUser, createUserBranches, deleteUserBranch }