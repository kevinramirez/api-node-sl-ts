import "dotenv/config";
import express, { Application } from "express";
import cors from "cors";
import routes from "./routes";
const app: Application = express();

const PORT = process.env.PORT || 30002;
const NODE_ENV = process.env.NODE_ENV || 'development';

app.use(cors({credentials: true, origin: '*'}));

app.use(express.json());

app.use('/b1s/v2', routes);

app.use(function(req, res, next) {
    return res.status(404).send({
        "error": {
            "status": 404,
            "msg": "The requested page does not exist."
        }
    })
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

if (NODE_ENV !== 'test') {
    app.listen(PORT, () => {
        console.log('API lista por el puerto ', PORT);
    });
}

export default app;