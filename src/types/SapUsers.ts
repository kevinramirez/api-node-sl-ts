export type branchesType = {
    ShipToCode: string;
}

export type SapUserType = {
    username?: string;
    password?: string;
    email: string;
    active?: number;
    first_name: string;
    last_name: string;
    FederalTaxID?: string;
    rolId: number;
    branches?: branchesType[];
}

/** Sap Users Branches */
export type SapBranchesType = {
    users_id: number;
    ShipToCode: string;
}